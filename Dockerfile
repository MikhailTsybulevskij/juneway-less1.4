FROM debian:10 as stage_0

RUN apt update &&\
 apt install -y wget unzip make libssl-dev libpcre3-dev gcc make zlib1g-dev perl &&\
 mkdir /tmp/nginx &&\
 cd /tmp/nginx &&\
 wget https://openresty.org/download/openresty-1.19.9.1.tar.gz &&\
 tar xvf openresty-1.19.9.1.tar.gz &&\
 cd /tmp/nginx/openresty-1.19.9.1 &&\
 ./configure &&\
 make install &&\
 ln -s /usr/local/openresty/nginx/sbin/nginx /sbin/nginx

FROM debian:10
WORKDIR /usr/local/openresty
COPY --from=stage_0 /usr/local/openresty* .
#COPY --from=stage_0 /lib/x86_64-linux-gnu/libdl.so.2 /lib/x86_64-linux-gnu/libdl.so.2
#COPY --from=stage_0 /lib/x86_64-linux-gnu/libpthread.so.0 /lib/x86_64-linux-gnu/libpthread.so.0
#COPY --from=stage_0 /lib/x86_64-linux-gnu/libcrypt.so.1 /lib/x86_64-linux-gnu/libcrypt.so.1
#COPY --from=stage_0 /usr/local/lib/libluajit-5.1.so.2 /usr/local/lib/libluajit-5.1.so.2
#COPY --from=stage_0 /lib/x86_64-linux-gnu/libm.so.6 /lib/x86_64-linux-gnu/libm.so.6
#COPY --from=stage_0 /lib/x86_64-linux-gnu/libpcre.so.3 /lib/x86_64-linux-gnu/libpcre.so.3
COPY --from=stage_0 /usr/lib/x86_64-linux-gnu/libssl.so.1.1 /usr/lib/x86_64-linux-gnu/libssl.so.1.1
COPY --from=stage_0 /usr/lib/x86_64-linux-gnu/libcrypto.so.1.1 /usr/lib/x86_64-linux-gnu/libcrypto.so.1.1
#COPY --from=stage_0 /lib/x86_64-linux-gnu/libz.so.1 /lib/x86_64-linux-gnu/libz.so.1
#COPY --from=stage_0 /lib/x86_64-linux-gnu/libc.so.6 /lib/x86_64-linux-gnu/libc.so.6
#COPY --from=stage_0 /lib/x86_64-linux-gnu/libgcc_s.so.1 /lib/x86_64-linux-gnu/libgcc_s.so.1
COPY --from=stage_0 /sbin/nginx /sbin/nginx

#RUN mkdir -p /var/log/nginx && touch /var/log/nginx/error.log && chmod +x nginx
CMD ["/sbin/nginx", "-g", "daemon off;"]
